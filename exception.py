import sentry_sdk
import rook
sentry_sdk.init(
    dsn="https://a995439b3461454a84298f8b7ffd794c@o4504013032128512.ingest.sentry.io/4504013032849408",
    _experiments={
        "custom_measurements": True,
    },
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

if __name__ == "__main__":
   rook.start(token='3c10a9414fa3108b21cc02cadc8b2352ece14c6d44628168e17778128c86b8d5',labels={"env":"dev"})

try:
    num = int(input("Enter any number: "))
except ValueError:
    print("Fine! I'll pick a number... 8")
    num = 8

print(f"You entered {num}")

