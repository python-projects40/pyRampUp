from textblob import TextBlob
import pyttsx3

engine = pyttsx3.init('sapi5')
engine.say("Hello, it's time to provide your employee statement!")
engine.runAndWait()


print("Enter your employee wellness statement: ")
statement = input(">")
blob = TextBlob(statement)
while blob.sentiment.polarity < 0.5:
    print("Please try to be positive!")
    engine.say("Please be more positive!")
    engine.runAndWait()
    statement = input(">")
    blob = TextBlob(statement)
print("Thanks for being positive!")
engine.say("Thanks for being positive! The ministry of work appreciates you!")
engine.runAndWait()